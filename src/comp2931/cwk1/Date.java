package comp2931.cwk1;
//by : Brian Max Tchouambe
//SID : 201066412
//Userame : sc16bmt

import java.util.Calendar;

//Coursework 1 class

public class Date
{
  private int year; //Declare variables
  private int month;
  private int day;

  public Date(int y, int m, int d)
  {
    year = y;
    month = m;
    day = d;
    validArgument(y, m, d);
  }

  //(Getter functions for year, month and date)
  public int getYear()
  {
    return year;
  }

  public int getMonth()
  {
    return month;
  }

  public int getDay()
  {
    return day;
  }

  //Override to string puts date in correct format
  @Override
  public String toString()
  {
    return String.format("%04d-%02d-%02d", year, month, day);
  }

  //if statements to throw error incase of an invalid date argument
  public void validArgument(int year, int month, int day)
  {
    if ((month == 4 || month == 6 || month == 9 || month == 11) && (day > 30 || (day < 1)))
    {
      throw new IllegalArgumentException("Argument Invalid");
    }

    if ((month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) && (day > 31 || (day < 1)))
    {
      throw new IllegalArgumentException("Argument Invalid");
    }

    if ((month < 1) || (month > 12))
    {
      throw new IllegalArgumentException("Argument Invalid");
    }
    //If it is february and it is a leap year and the date is greater than 29, throws invalid
    if ((month == 2) && (year%4 == 0) && (day > 29 || (day < 1)))
    {
      throw new IllegalArgumentException("Argument Invalid");
    }

    if ((year < 1))
    {
      throw new IllegalArgumentException("Argument Invalid");
    }
    //If it is february and it is not a leap year and the date is greater than 28, throws invalid
    if ((month == 2) && (year%4 != 0) && (day > 28 || (day < 1)))
    {
      throw new IllegalArgumentException("Argument Invalid");
    }
  }

  //Checks objects and returns true if equal and false otherwise
  @Override
  public boolean equals(Object candy)
  {
    if (candy == this)
    {
      return true;
    }

    else if (!(candy instanceof Date))
    {
      return false;
    }

    else
    {
      Date candyDate = (Date) candy;
      return getDay() == candyDate.getDay() && getMonth() == candyDate.getMonth() && getYear() == candyDate.getYear();
    }
  }

  //Gets date of the year
  public int getdoy()
  {
    //Array of all the maximum days in each month when its not a leap year
    int[] marraynl = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    //Array of all the maximum days in each month when its a leap year
    int[] marrayl = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int a;
    int b = 0;
    int c = this.getMonth();
    int doy = this.getDay();
    if (year%4 != 0)
    {
      for (a = 0; a < c - 1; a++)
      {
        b += marraynl[a];
      }

      b += doy;
      return b;
    }
    else
    {
      for (a = 0; a < c - 1; a++)
      {
        b += marrayl[a];
      }

      b += doy;
      return b;
    }
  }

}
