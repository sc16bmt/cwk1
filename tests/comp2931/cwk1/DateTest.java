package comp2931.cwk1;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class DateTest
{
  //Declare variables
  private Date CD;
  private Date LD;

  //Rule command
  @Rule
  public ExpectedException exception = ExpectedException.none();

  //Assigns date to variables
  @Before
  public void setUp() throws IllegalAccessException
  {
    CD = new Date(2048, 01, 21);
    LD = new Date(2048, 01, 21);
  }

  //Tests the string date function
  @Test
  public void stringdate()
  {
    assertThat(CD.toString(), is("2048-01-21"));
  }

  //Tests the current date function
  @Test
  public void currentd()
  {
    CD = new Date(2048, 01, 21);
    assertThat(LD.getYear(), is(2048));
    assertThat(LD.getMonth(), is(01));
    assertThat(LD.getDay(), is(21));
  }

  //Tests if exception is thrown when year is too low and expected results
  @Test(expected = IllegalArgumentException.class)
  public void LY()
  {
    new Date(0000, 01, 21);
  }

  //Tests if exception is thrown when month is too low and expected results
  @Test(expected = IllegalArgumentException.class)
  public void LM()
  {
    new Date(2048, 00, 21);
  }

  //Tests if exception is thrown when month is too high and expected results
  @Test(expected = IllegalArgumentException.class)
  public void HM()
  {
    new Date(2048, 21, 21);
  }

  //Tests if exception is thrown when day is too low and expected results
  @Test(expected = IllegalArgumentException.class)
  public void LD()
  {
    new Date(2048, 01, 0);
  }

  //Tests if exception is thrown when day is too high and expected results
  @Test(expected = IllegalArgumentException.class)
  public void HD()
  {
    new Date(2048, 01, 45);
  }

  //Tests the equality of different statments comparing the date SD with others
  @Test
  public void equal()
  {
    Date SD = new Date(2010, 10, 10);

    assertFalse(SD.equals(equalTo(new Date(2047, 10, 8))));
    assertFalse(SD.equals(new Date(2014, 9, 20)));
    assertTrue(SD.equals(SD));
    assertFalse(SD.equals(new Date(1847, 11, 2)));
    assertFalse(SD.equals("2012-07-05"));
    assertFalse(SD.equals(equalTo(new Date(0003, 01, 12))));
  }

  //Tests if getdoy function works
  @Test
  public void getdoy()
  {
    Date doyDate = new Date(2017, 04, 26);
    assertThat(doyDate.getdoy(), is(116));
  }
  //Tests if leapyear implimentation works. Fails if not a leap year
  @Test
  public void Leapyear()
  {
    Date sixhJanuary = new Date(2016, 01, 06);
    assertThat(sixhJanuary.getdoy(), is(6));
    assertThat(sixhJanuary.toString(), is("2016-01-06"));
  }

  //Checks for todays date
  @Test
  public void td()
  {
    Date td;
    td = new Date(2037, 10, 19);

    assertEquals(td, td);
    assertThat(td, is(new Date(2037, 10, 19)));
  }

}
